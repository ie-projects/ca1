package org.fn.ie;

import java.util.ArrayList;

public class Restaurant {
    private String name;
    private String description;
    private Location location;
    private ArrayList<Food> menu;

//    public Restaurant(String name, String description, int x, int y, ArrayList<>)

    public void addFood(Food newFood)
    {
        menu.add(newFood);
    }

    public String getName() { return name; }

    public String getDescription() { return description; }

    public ArrayList<Food> getMenu() { return menu; }

    public Location getLocation() { return location; }
}
