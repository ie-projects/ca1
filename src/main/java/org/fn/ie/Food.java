package org.fn.ie;

public class Food {
    private String name;
    private String description;
    private float popularity;
    private String restaurantName;
    private int price;

    public String getName() { return name; }

    public String getDescription() { return description; }

    public float getPopularity() { return popularity; }

    public String getRestaurantName() { return restaurantName; }

    public int getPrice() { return price; }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
