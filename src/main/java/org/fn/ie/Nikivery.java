package org.fn.ie;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import  java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Nikivery {
    private ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
    private String cartRestaurant;
    private ArrayList<Food> cart = new ArrayList<Food>();

    private ObjectMapper mapper = new ObjectMapper();

    public class Order {
        private String foodName;
        private int count;

        public Order(String foodName, int count)
        {
            this.foodName = foodName;
            this.count = count;
        }

        public String getFoodName() { return foodName; }

        public int getCount() { return count; }
    }

    public class RestaurantScore {
        private String restaurantName;
        private double score;

        public RestaurantScore(String restaurantName, double score)
        {
            this.restaurantName = restaurantName;
            this.score = score;
        }

        public double getScore() { return score; }

        public String getRestaurantName() { return restaurantName; }
    }

    private ArrayList<Order> organizeOrders()
    {
        ArrayList<Order> orders = new ArrayList<Order>();
        for(int i = 0; i < cart.size(); i++)
        {
            int foodCount = 1;
            boolean repetitive = false;
            for(int j =0; j < cart.size(); j++)
                if(cart.get(i).getName().equals(cart.get(j).getName()))
                {
                    if(i == j)
                        continue;
                    if(j < i)
                    {
                        repetitive = true;
                        break;
                    }
                    foodCount++;
                }
            if(!repetitive)
                orders.add(new Order(cart.get(i).getName(), foodCount));
        }
        return orders;
    }

    private double evaluateScore(Restaurant restaurant)
    {
        double avgFoodPopularity = 0;
        double distance;
        for(int i = 0; i < restaurant.getMenu().size(); i++)
            avgFoodPopularity += restaurant.getMenu().get(i).getPopularity();
        avgFoodPopularity /= restaurant.getMenu().size();
        distance = Math.hypot(restaurant.getLocation().getX(), restaurant.getLocation().getY());
        return avgFoodPopularity / distance;
    }

    private String serializeObject(Object obj)
    {
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            System.out.println(e);
            return "Error";
        }
    }

    public void addRestaurant(String serialRestaurantInfo)
    {
        try {
            Restaurant newRestaurant = mapper.readValue(serialRestaurantInfo, Restaurant.class);
            for(int i = 0; i < newRestaurant.getMenu().size(); i++)
                newRestaurant.getMenu().get(i).setRestaurantName(newRestaurant.getName());
            restaurants.add(newRestaurant);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void addFood(String serialFoodInfo)
    {
        try {
            Food newFood = mapper.readValue(serialFoodInfo, Food.class);
            for (Restaurant restaurant : restaurants) {
                if (newFood.getRestaurantName().equals(restaurant.getName())) {
                    restaurant.addFood(newFood);
                    return;
                }
            }
            System.out.println("Restaurant not found!");

        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void getRestaurants()
    {
        for(Restaurant restaurant : restaurants)
        {
            System.out.println(restaurant.getName());
        }
    }

    public void getRestaurant(String serialRestaurantName)
    {
        try {
            final ObjectNode node = new ObjectMapper().readValue(serialRestaurantName, ObjectNode.class);
            String restaurantName = String.valueOf(node.get("name"));
            restaurantName = restaurantName.substring(1, restaurantName.length()-1);
            for(Restaurant restaurant : restaurants)
            {
                if(restaurant.getName().equals(restaurantName))
                {
                    System.out.println(serializeObject(restaurant));
                    return;
                }
            }
            System.out.println("Restaurant not found!");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void getFood(String serialFoodRestaurantName)
    {
        try {
            final ObjectNode node = new ObjectMapper().readValue(serialFoodRestaurantName, ObjectNode.class);
            String restaurantName = String.valueOf(node.get("restaurantName"));
            restaurantName = restaurantName.substring(1, restaurantName.length()-1);
            String foodName = String.valueOf(node.get("foodName"));
            foodName = foodName.substring(1, foodName.length()-1);
            for(Restaurant restaurant : restaurants)
            {
                if(restaurant.getName().equals(restaurantName))
                {
                    for(Food food : restaurant.getMenu())
                    {
                        if(food.getName().equals(foodName))
                        {
                            System.out.println(serializeObject(food));
                            return;
                        }
                    }
                    System.out.println("Food not found!");
                    return;
                }
            }
            System.out.println("Restaurant not found!");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void addToCart(String serialFoodRestaurantName)
    {
        try {
            final ObjectNode node = new ObjectMapper().readValue(serialFoodRestaurantName, ObjectNode.class);
            String restaurantName = String.valueOf(node.get("restaurantName"));
            restaurantName = restaurantName.substring(1, restaurantName.length()-1);
            String foodName = String.valueOf(node.get("foodName"));
            foodName = foodName.substring(1, foodName.length()-1);
            for(Restaurant restaurant : restaurants)
            {
                if(restaurant.getName().equals(restaurantName))
                {
                    for(Food food : restaurant.getMenu())
                    {
                        if(food.getName().equals(foodName))
                        {
                            if(cart.size() == 0 || cartRestaurant.equals(restaurantName))
                            {
                                cart.add(food);
                                cartRestaurant = restaurantName;
                            }
                            else
                            {
                                System.out.println("Cart contains food from other restaurants!");
                            }
                            return;
                        }
                    }
                    System.out.println("Food not found!");
                    return;
                }
            }
            System.out.println("Restaurant not found!");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void getCart()
    {
        ArrayList<Order> orders = organizeOrders();
        System.out.println(serializeObject(orders));
    }

    public void finalizeOrder()
    {
        getCart();
        System.out.println("Orders submitted!");
        cart.clear();
    }

    public void getRecommendedRestaurants()
    {
        ArrayList<RestaurantScore> restaurantScores = new ArrayList<RestaurantScore>();
        for(Restaurant restaurant : restaurants)
            restaurantScores.add(new RestaurantScore(restaurant.getName(), evaluateScore(restaurant)));
        restaurantScores.sort(Comparator.comparingDouble(RestaurantScore::getScore));
        if(restaurantScores.size() > 0)
            System.out.println("1. " + restaurantScores.get(restaurantScores.size()-1).getRestaurantName());
        if(restaurantScores.size() > 1)
            System.out.println("2. " + restaurantScores.get(restaurantScores.size()-2).getRestaurantName());
        if(restaurantScores.size() > 2)
            System.out.println("3. " + restaurantScores.get(restaurantScores.size()-3).getRestaurantName());
    }

    public static void main(String[] args)
    {
        Nikivery nikivery = new Nikivery();
        String command;
        String commandArg;
        Scanner stdin = new Scanner(System.in);
        while(true)
        {
            command = stdin.next();
            commandArg = stdin.nextLine();
            commandArg = commandArg.trim();
            switch (command)
            {
                case "addRestaurant":
                    nikivery.addRestaurant(commandArg);
                    break;
                case "addFood":
                    nikivery.addFood(commandArg);
                    break;
                case "getRestaurants":
                    nikivery.getRestaurants();
                    break;
                case "getRestaurant":
                    nikivery.getRestaurant(commandArg);
                    break;
                case "getFood":
                    nikivery.getFood(commandArg);
                    break;
                case "addToCart":
                    nikivery.addToCart(commandArg);
                    break;
                case "getCart":
                    nikivery.getCart();
                    break;
                case "finalizeOrder":
                    nikivery.finalizeOrder();
                    break;
                case "getRecommendedRestaurants":
                    nikivery.getRecommendedRestaurants();
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Command unknown!");
            }
        }
    }

}
//addRestaurant {"name": "Hesturan", "description": "luxury", "location": {"x": 1, "y": 3}, "menu": [{"name": "Gheime", "description": "it’s yummy!", "popularity": 0.8, "price": 20000}, {"name": "Kabab", "description": "it’s delicious!", "popularity": 0.6, "price": 30000}]}
//addRestaurant {"name": "boof", "description": "fast food", "location": {"x": 2, "y": 4}, "menu": [{"name": "Pizza", "description": "Nice", "popularity": 1, "price": 50000}, {"name": "Spaghetti", "description": "Italian", "popularity": 0.9, "price": 30}]}
//addRestaurant {"name": "gelato", "description": "ice cream", "location": {"x": 6, "y": 4}, "menu": [{"name": "Ferrero", "description": "Good", "popularity": 0.1, "price": 500}]}
//addRestaurant {"name": "Doner garden", "description": "Good", "location": {"x": 1, "y": 1}, "menu": [{"name": "Doner", "description": "Good", "popularity": 1.3, "price": 800}]}
//addFood { "name": "Abgusht", "description" :"good" ,"popularity" : 3.4, "restaurantName" :"Hesturan", "price": 300}
//getRestaurant {"name" : "Hesturan"}
//getFood {"foodName": "Kabab", "restaurantName": "Hesturan"}
//addToCart {"foodName": "Kabab", "restaurantName": "Hesturan"}
//addToCart {"foodName": "Pizza", "restaurantName": "boof"}