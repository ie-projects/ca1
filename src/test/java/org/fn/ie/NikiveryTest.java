package org.fn.ie;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class NikiveryTest {
    private Nikivery nikivery;
    private ByteArrayOutputStream outContent;

    @Before
    public void setup()
    {
        nikivery = new Nikivery();
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        nikivery.addRestaurant(
                "{\"name\": \"Hesturan\", \"description\": \"luxury\", \"location\":" +
                        " {\"x\": 1, \"y\": 3}, \"menu\": [{\"name\": \"Gheime\", \"description\": \"it’s yummy!\"," +
                        " \"popularity\": 0.8, \"price\": 20000}, {\"name\": \"Kabab\"," +
                        " \"description\": \"it’s delicious!\", \"popularity\": 0.6, \"price\": 30000}]}"
        ); // score: 0.5059
        nikivery.addRestaurant(
                "{\"name\": \"Boof\", \"description\": \"fast food\", \"location\":" +
                        " {\"x\": 2, \"y\": 4}, \"menu\": [{\"name\": \"Pizza\", \"description\": \"Nice\"," +
                        " \"popularity\": 1, \"price\": 50000}, {\"name\": \"Spaghetti\"," +
                        " \"description\": \"Italian\", \"popularity\": 0.9, \"price\": 30}]}"
        ); // score: 0.2124
        nikivery.addRestaurant(
                "{\"name\": \"Gelato\", \"description\": \"ice cream\", \"location\":" +
                        " {\"x\": 6, \"y\": 4}, \"menu\": [{\"name\": \"Ferrero\", \"description\": \"Good\"," +
                        " \"popularity\": 0.1, \"price\": 500}]}"
        ); // score: 0.0138
        nikivery.addRestaurant(
                "{\"name\": \"Doner garden\", \"description\": \"Good\", \"location\":" +
                        " {\"x\": 1, \"y\": 1}, \"menu\": [{\"name\": \"Doner\", \"description\": \"Good\"," +
                        " \"popularity\": 1.3, \"price\": 800}]}"
        ); // score: 0.9192

        nikivery.addFood(
                "{ \"name\": \"Abgusht\", \"description\" :\"Good\" ,\"popularity\" : 3.4," +
                        " \"restaurantName\" :\"Hesturan\", \"price\": 300}"
        );

        nikivery.addToCart(
                "{\"foodName\": \"Kabab\", \"restaurantName\": \"Hesturan\"}"
        );
        nikivery.addToCart(
                "{\"foodName\": \"Abgusht\", \"restaurantName\": \"Hesturan\"}"
        );
        nikivery.addToCart(
                "{\"foodName\": \"Kabab\", \"restaurantName\": \"Hesturan\"}"
        );

    }

    @Test
    public void testGetRecommendedRestaurants()
    {
        nikivery.getRecommendedRestaurants();
        assertEquals("1. Doner garden\r\n2. Hesturan\r\n3. Boof\r\n", outContent.toString());
    }

    @Test
    public void testFinalizeOrder()
    {
        nikivery.finalizeOrder();
        assertEquals("[{\"foodName\":\"Kabab\",\"count\":2},{\"foodName\":\"Abgusht\",\"count\":1}]\r\n" +
                "Orders submitted!\r\n", outContent.toString());
    }
}
