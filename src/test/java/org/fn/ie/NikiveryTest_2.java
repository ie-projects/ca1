package org.fn.ie;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class NikiveryTest_2 {
    private Nikivery nikivery;
    private ByteArrayOutputStream outContent;

    @Before
    public void setup()
    {
        nikivery = new Nikivery();
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        nikivery.addRestaurant(
                "{\"name\": \"ResA\", \"description\": \"luxury\", \"location\":" +
                        " {\"x\": 10, \"y\": 13}, \"menu\": [{\"name\": \"Gheime\", \"description\": \"it’s yummy!\"," +
                        " \"popularity\": 0.8, \"price\": 20000}, {\"name\": \"Kabab\"," +
                        " \"description\": \"it’s delicious!\", \"popularity\": 0.6, \"price\": 30000}]}"
        ); // score: 0.0426
        nikivery.addRestaurant(
                "{\"name\": \"ResB\", \"description\": \"fast food\", \"location\":" +
                        " {\"x\": 3, \"y\": 4}, \"menu\": [{\"name\": \"Pizza\", \"description\": \"Nice\"," +
                        " \"popularity\": 1, \"price\": 50000}, {\"name\": \"Spaghetti\"," +
                        " \"description\": \"Italian\", \"popularity\": 0.9, \"price\": 30}]}"
        ); // score: 0.19
        nikivery.addRestaurant(
                "{\"name\": \"ResC\", \"description\": \"ice cream\", \"location\":" +
                        " {\"x\": 1, \"y\": 2}, \"menu\": [{\"name\": \"Ferrero\", \"description\": \"Good\"," +
                        " \"popularity\": 0.1, \"price\": 500}]}"
        ); // score: 0.0447
        nikivery.addRestaurant(
                "{\"name\": \"ResD\", \"description\": \"Good\", \"location\":" +
                        " {\"x\": 2, \"y\": 2}, \"menu\": [{\"name\": \"Doner\", \"description\": \"Good\"," +
                        " \"popularity\": 6, \"price\": 800}]}"
        ); // score: 2.1213

        nikivery.addRestaurant(
                "{\"name\": \"ResE\", \"description\": \"Good\", \"location\":" +
                        " {\"x\": 6, \"y\": 7}, \"menu\": [{\"name\": \"Doner\", \"description\": \"Good\"," +
                        " \"popularity\": 0.5, \"price\": 800}]}"
        ); // score: 0.0542

        nikivery.addRestaurant(
                "{\"name\": \"ResF\", \"description\": \"Good\", \"location\":" +
                        " {\"x\": 10, \"y\": 5}, \"menu\": [{\"name\": \"Doner\", \"description\": \"Good\"," +
                        " \"popularity\": 1.15, \"price\": 800}]}"
        ); // score: 0.1029

        nikivery.addRestaurant(
                "{\"name\": \"ResG\", \"description\": \"Good\", \"location\":" +
                        " {\"x\": 4, \"y\": 9}, \"menu\": [{\"name\": \"Doner\", \"description\": \"Good\"," +
                        " \"popularity\": 47, \"price\": 800}]}"
        ); // score: 4.7721

        nikivery.addToCart(
                "{\"foodName\": \"Kabab\", \"restaurantName\": \"ResA\"}"
        );
        nikivery.addToCart(
                "{\"foodName\": \"Gheime\", \"restaurantName\": \"ResA\"}"
        );
        nikivery.addToCart(
                "{\"foodName\": \"Kabab\", \"restaurantName\": \"ResA\"}"
        );
        nikivery.addToCart(
                "{\"foodName\": \"Kabab\", \"restaurantName\": \"ResA\"}"
        );

    }

    @Test
    public void testGetRecommendedRestaurants()
    {
        nikivery.getRecommendedRestaurants();
        assertEquals("1. ResG\r\n2. ResD\r\n3. ResB\r\n", outContent.toString());
    }

    @Test
    public void testFinalizeOrder()
    {
        nikivery.finalizeOrder();
        assertEquals("[{\"foodName\":\"Kabab\",\"count\":3},{\"foodName\":\"Gheime\",\"count\":1}]\r\n" +
                "Orders submitted!\r\n", outContent.toString());
    }
}
